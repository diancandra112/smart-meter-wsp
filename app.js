require('dotenv').config()

const express = require('express')
const app = express()
const bodyparser = require('body-parser')
const logger = require('morgan')
const cors = require('cors')

const PORT = process.env.PORT || 80

const routes = require('./routes/routes')
app.use(cors())

app.listen(PORT,()=>{
    console.log(`Server Running At Port ${PORT}`);
})

app.use(logger('dev'))
app.use(bodyparser.json())
app.use(bodyparser.urlencoded({extended:false}))

app.use('/',routes)